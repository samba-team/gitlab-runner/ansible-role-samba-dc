---
# A playbook to set up a samba server as AD DC
# ref: https://wiki.samba.org/index.php/Setting_up_Samba_as_an_Active_Directory_Domain_Controller

- wait_for_connection:
  tags: always

- setup:
  tags: always

- debug: var=vars
- debug: var=hostvars

- name: use pdc group to find primary dc info if defined
  tags: always
  when: '"pdc" in group_names'
  set_fact:
    is_primary_dc: yes
    primary_dc_name: "{{inventory_hostname}}"

- name: ensure primary_dc_name is defined
  when: primary_dc_name is not defined
  fail:
    msg: "primary_dc_name is required"

- name: ensure primary_dc_name is valid
  when: primary_dc_name not in hostvars
  fail:
    msg: "primary_dc_name {{primary_dc_name}} is not a valid host"

- name: update is_primary_dc by primary_dc_name
  tags: always
  set_fact:
    is_primary_dc: "{{primary_dc_name|lower == inventory_hostname|lower}}"

- name: set fact for primary_dc_ip if openstack
  # for openstack, we use private ipv4 in network
  tags: always
  set_fact:
    primary_dc_ip: '{{hostvars[primary_dc_name]["openstack"]["private_v4"]}}'
  when:
    - 'openstack is defined'

- name: set fact for primary_dc_ip if not openstack
  # for env like vagrant, just use ansible_host as ip
  tags: always
  set_fact:
    primary_dc_ip: '{{hostvars[primary_dc_name].ansible_host}}'
  when:
    - 'openstack is not defined'
    - 'hostvars[primary_dc_name] is defined'
    - '"ansible_host" in hostvars[primary_dc_name]'

- name: set fact for ipv4_address if openstack
  set_fact:
    ipv4_address: '{{openstack["private_v4"]}}'
  when:
    - 'ipv4_address is not defined'
    - 'openstack is defined'
  tags:
    - always

- name: set fact for ipv4_address if not openstack
  # for env like vagrant, just use ansible_host as ip
  tags: always
  set_fact:
    ipv4_address: '{{ansible_host}}'
  when:
    - 'ipv4_address is not defined'
    - 'openstack is not defined'

- name: disable resolvconf
  become: yes
  service:
    name: resolvconf
    enabled: no
  failed_when: no

- name: make sure /etc/hosts resolves DC's FQDN and short name to LAN IP
  become: yes
  lineinfile:
    path: "/etc/hosts"
    line: "{{ipv4_address}} {{ansible_hostname}}.{{samba_realm}} {{ansible_hostname}}"

- name: kill existing samba service and its friends
  become: yes
  command: "killall --quiet --wait samba smbd nmdb winbindd"
  failed_when: no

- name: make sure samba supervisor service stopped before domain operations
  become: yes
  supervisorctl:
    name: samba
    state: stopped
  failed_when: no
  # service may not stated yet, ignore any error

- name: remove existing samba files from previous installation if any
  become: yes
  file:
    path: "{{ samba_install_dir }}/{{ item }}"
    state: absent
  with_items:
    - etc/smb.conf
    - var/lock
    - var/locks
    - var/cache
    - private

- name: remove existing /etc/krb5.conf file
  become: yes
  file:
    path: "/etc/krb5.conf"
    state: absent

- name: include tasks/mit-krb5.yml if enabled
  include_tasks: tasks/mit-krb5.yml
  when: mit_krb5_enabled is defined and mit_krb5_enabled

- name: Create a link to krb5.conf
  become: yes
  file:
    src: /usr/local/samba/private/krb5.conf
    dest: /etc/krb5.conf
    force: true
    state: link
    owner: root
    group: root

- name: render /etc/resolv.conf
  become: yes
  copy:
    dest: /etc/resolv.conf
    content: |
      nameserver {{primary_dc_ip}}
      nameserver 8.8.8.8
      search {{samba_realm}}
  tags:
    - render-resolv-conf

- name: create samba dirs
  become: yes
  file:
    path: "/usr/local/samba/var/locks"
    state: directory

- name: make install
  become: yes
  command: make install
  args:
    chdir: "{{ samba_repo_root }}"

- name: include nasswitch tasks
  include_tasks: nsswitch.yml

- name: update PATH for samba
  become: yes
  lineinfile:
    path: "/etc/environment"
    regexp: '^PATH='
    line: "PATH={{samba_install_dir}}/sbin:{{samba_install_dir}}/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

- name: primary dc domain provision
  when:
    - is_primary_dc
    - 'samba_backup_file is not defined'
  include_tasks: tasks/pdc-provision.yml

- name: primary dc domain restore
  when:
    - is_primary_dc
    - 'samba_backup_file is defined'
  include_tasks: tasks/pdc-restore.yml
  vars:
    samba_data_dir: "{{ samba_install_dir}}/restore"

- name: populate db with samba-cli.py if required
  when:
    - is_primary_dc
    - samba_populate_db is defined
    - samba_populate_db|bool
  become: true
  command: >
    python3 samba-cli.py --verbose populate
    --num-users {{ num_users|int }}
    --num-groups {{ num_groups|int }}
    --num-groups-per-user {{ num_groups_per_user|int }}
  environment:
    SAMBA_REPO_ROOT: "{{ samba_repo_root }}"
  args:
    chdir: "{{ansible_env.HOME}}"

- name: generate users and groups if required
  when:
    - is_primary_dc
    - generate_users_and_groups|bool
  become: yes
  block:
    - name: render generate-users-and-groups.sh
      template:
        src: templates/generate-users-and-groups.sh
        # this scipt must be in repo root
        dest: "{{samba_repo_root}}/generate-users-and-groups.sh"
        mode: 0755

    - name: run generate-users-and-groups.sh
      command: "./generate-users-and-groups.sh /usr/local/samba/private/sam.ldb"
      args:
        chdir: "{{samba_repo_root}}"

    - name: create a samba backup file
      command: "/usr/local/samba/bin/samba-tool domain backup offline --targetdir={{ansible_env.HOME}}/backup"

- name: backup dc domain join
  when: not is_primary_dc
  include_tasks: tasks/bdc-join.yml
