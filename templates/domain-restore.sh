#!/bin/bash

set -xue

{{samba_install_dir}}/bin/samba-tool domain backup restore \
  --newservername={{primary_dc_name}} \
  --targetdir={{samba_data_dir}} \
  --username={{samba_username}} \
  --password={{samba_password}} \
  --option='winbind enum users = yes' \
  --option='winbind enum groups = yes' \
  --backup-file={{ansible_env.HOME}}/samba-backup

# somehow it could happen that the parent dir doesn't exist
mkdir -p {{samba_data_dir}}/private/tls

# password may expire in the backup file, reset it
{{samba_install_dir}}/bin/samba-tool user setpassword {{samba_username}} \
  --newpassword {{samba_password}} \
  --configfile {{samba_data_dir}}/etc/smb.conf

# do not expire password in domain, then we can use any other uses
{{samba_install_dir}}/bin/samba-tool domain passwordsettings \
    set --max-pwd-age=0 \
    --configfile {{samba_data_dir}}/etc/smb.conf

# link conf file to default location so we don't need to provide it
ln -sf {{samba_data_dir}}/etc/smb.conf {{samba_install_dir}}/etc/smb.conf
