#!/bin/bash -x

# call with dns-hostname:
# joetmp2-dc-0.SAMDOM.EXAMPLE.COM
# or /path/to/sam.ldb:
# /usr/local/samba/private/sam.ldb

# for old version like v4-9,
# rm --max-members which doesn't exist
# and the host can not be ldb path

sudo ./script/traffic_replay -d 3 \
    --username {{samba_username}} \
    --password {{samba_password}}  \
    --realm {{samba_realm}} \
    --workgroup {{samba_domain|upper}} \
    --fixed-password iegh1haevoofoo3looT9  \
    --random-seed=1 \
    --option='ldb:nosync=true' \
    --generate-users-only \
    --number-of-users={{num_users|int}} \
    --number-of-groups={{num_groups|int}} \
    --average-groups-per-user={{num_groups_per_user|int}} \
    --max-members={{num_max_members|int}} \
    "$@"
