#!/bin/bash

set -xue

rm -rf /usr/local/samba/private

/usr/local/samba/bin/samba-tool domain \
  join {{samba_domain}} {{domain_role}} \
  --server={{primary_dc_name}} \
  --backend-store={{samba_backend_store}} \
  --realm={{samba_realm}} \
  --adminpass={{samba_password}} \
  --username={{samba_username}} \
  --password={{samba_password}}
